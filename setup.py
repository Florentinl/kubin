from setuptools import setup, find_packages


setup(name='Kubin',
      version='0.2',
      description="Rubik's Cube model and solver",
      url='http://github.com/Edify-Multitude/Kubin',
      author='Edify-Multitude',
      author_email='',
      license='',
      packages=find_packages(exclude=["Kubin"]),
      zip_safe=False)
